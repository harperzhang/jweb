package com.gdmob.jweb.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.gdmob.jweb.common.SplitPage;
import com.gdmob.jweb.config.Constants;
import com.gdmob.jweb.model.Soft;
import com.gdmob.jweb.model.Upload;
import com.jfinal.upload.UploadFile;

public class SoftService extends BaseService {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(SoftService.class);

	public static final SoftService service = new SoftService();
	/**
	 * 分页
	 * @param splitPage
	 */
	public void list(SplitPage splitPage){
		String select = "select f.* ";
		splitPageBase(splitPage, select, "pingtai.soft.splitPage");
	}
	public void delete(String ids){
		try{
			Soft soft = Soft.dao.findById(ids);
			if(soft!=null){
				String fileIds = soft.getStr("soft");
				Upload.dao.deleteAll(fileIds);
			}
			// 删除
			Soft.dao.deleteById(ids);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public Soft update(UploadFile uploadFile,Soft soft){
		if(uploadFile!=null){
			String ids = soft.getStr("ids");
			if(StringUtils.isNotBlank(ids)){
				Soft oldSoft = Soft.dao.findById(ids);
				//删除原来的软件包
				String oldFile = oldSoft.getStr("soft");
				Upload.dao.deleteAll(oldFile);
			}
			String fileIds = getDestFile(uploadFile, Constants.SOFT);
			soft.set("soft",fileIds);
		}else{
			soft.remove("soft");
		}
		Integer force = soft.getInt("force");
		if(force==null||force!=1){
			soft.set("force",0);
		}else{
			soft.set("force",1);
		}
		Integer status = soft.getInt("status");
		if(status==null||status!=1){
			soft.set("status",0);
		}else{
			soft.set("status",1);
		}
		soft.updateModel();
		return soft;
	}
	
}
