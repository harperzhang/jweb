package com.gdmob.jweb.service;

import org.apache.log4j.Logger;

import com.gdmob.jweb.common.SplitPage;
import com.jfinal.plugin.activerecord.Db;

public class FeedbackService extends BaseService {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(FeedbackService.class);

	public static final FeedbackService service = new FeedbackService();
	/**
	 * 分页
	 * @param splitPage
	 */
	public void list(SplitPage splitPage){
		String select = "select f.ids,f.fb1,f.fb2,f.fb3,f.fb4,f.other,f.version,f.type,f.createtime,s.name as salonId,a.phone as accountId ";
		splitPageBase(splitPage, select, "pingtai.feedback.splitPage");
	}
	public long getCount(String index){
		String c = "select count(*) from tdc_feedback where "+index + "=1";
		long count = Db.queryLong(c);
		return count;
	}
}
