package com.gdmob.jweb.service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.gdmob.jweb.common.SplitPage;
import com.gdmob.jweb.model.User;
import com.gdmob.jweb.model.UserInfo;
import com.gdmob.jweb.tools.ToolSecurityPbkdf2;
import com.gdmob.jweb.tools.ToolSqlXml;

public class UserService extends BaseService {

	private static Logger log = Logger.getLogger(UserService.class);

	public static final UserService service = new UserService();
	
	/**
	 * 保存
	 * 
	 * @param user
	 * @param passWord
	 * @param userInfo
	 */
	public void save(User user, String password, UserInfo userInfo) {
		try {
			// 密码加密
			byte[] salt = ToolSecurityPbkdf2.generateSalt();// 密码盐
			byte[] encryptedPassword = ToolSecurityPbkdf2.getEncryptedPassword(password, salt);
			user.set("salt", salt);
			user.set("password", encryptedPassword);

			// 保存用户信息
			userInfo.save();

			// 保存用户
			user.set("userinfoids", userInfo.getStr("ids"));
			user.set("errorcount", 0);
			user.set("status", "1");
			user.save();

			// 缓存
			User.dao.cacheAdd(user.getStr("ids"));
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("保存用户密码加密操作异常");
		} catch (InvalidKeySpecException e) {
			throw new RuntimeException("保存用户密码加密操作异常");
		} catch (Exception e) {
			throw new RuntimeException("保存用户异常");
		}
	}

	/**
	 * 更新
	 * 
	 * @param user
	 * @param passWord
	 * @param userInfo
	 */
	public void update(User user, String password, UserInfo userInfo) {
		try {
			// 密码加密
			if (null != password && !password.trim().equals("")) {
				User oldUser = User.dao.findById(user.getStr("ids"));
				byte[] salt = oldUser.getBytes("salt");// 密码盐
				byte[] encryptedPassword = ToolSecurityPbkdf2.getEncryptedPassword(password, salt);
				user.set("password", encryptedPassword);
			}

			// 更新用户
			user.update();
			userInfo.update();

			// 缓存
			User.dao.cacheAdd(user.getStr("ids"));
		} catch (Exception e) {
			throw new RuntimeException("更新用户异常");
		}
	}

	/**
	 * 删除
	 * 
	 * @param userIds
	 */
	public void delete(String userIds) {
		User user = User.dao.findById(userIds);
		String userInfoIds = user.getStr("userinfoids");

		// 缓存
		User.dao.cacheRemove(userIds);

		// 删除
		User.dao.deleteById(userIds);
		UserInfo.dao.deleteById(userInfoIds);
	}

	/**
	 * 设置用户所在的组
	 * 
	 * @param userIds
	 * @param groupIds
	 */
	public void setGroup(String userIds, String groupIds) {
		User user = User.dao.findById(userIds);
		user.set("groupids", groupIds).update();

		// 缓存
		User.dao.cacheAdd(userIds);
	}


	/**
	 * 分页
	 * 
	 * @param splitPage
	 */
	public void list(SplitPage splitPage) {
		String select = " select u.ids, u.username, ui.names, ui.email, ui.mobile, ui.birthday ";
		splitPageBase(splitPage, select, "pingtai.user.splitPage");
	}

	/**
	 * 验证密码是否正确
	 * @param userName
	 * @param passWord
	 * @return
	 */
	public boolean valiPassWord(String userName, String passWord) {
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("column", "userName");
			String sql = ToolSqlXml.getSql("pingtai.user.column", param);
			User user = User.dao.findFirst(sql, userName);
			byte[] salt = user.getBytes("salt");// 密码盐
			byte[] encryptedPassword = user.getBytes("password");
			boolean bool = ToolSecurityPbkdf2.authenticate(passWord, encryptedPassword, salt);
			if (bool) {
				return true;
			}
		} catch (Exception e) {
			log.error("验证密码是否正确异常，userName:" + userName + "，密码：" + passWord);
			return false;
		}
		return false;
	}
	
	/**
	 * 密码变更
	 * @param userName
	 * @param passOld
	 * @param passNew
	 */
	public void passChange(String userName, String passOld, String passNew){
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("column", "userName");
			String sql = ToolSqlXml.getSql("pingtai.user.column", param);
			User user = User.dao.findFirst(sql, userName);
			
			// 验证密码
			byte[] salt = user.getBytes("salt");// 密码盐
			byte[] encryptedPassword = user.getBytes("password");
			boolean bool = false;
			try {
				bool = ToolSecurityPbkdf2.authenticate(passOld, encryptedPassword, salt);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				e.printStackTrace();
			}
			if (bool) {
				byte[] saltNew = ToolSecurityPbkdf2.generateSalt();// 密码盐
				byte[] encryptedPasswordNew = ToolSecurityPbkdf2.getEncryptedPassword(passNew, saltNew);
				user.set("salt", saltNew);
				user.set("password", encryptedPasswordNew);
				// 更新用户
				user.update();
				// 缓存
				User.dao.cacheAdd(user.getStr("ids"));
			}
		} catch (Exception e) {
			log.error("更新用户密码异常，userName:" + userName + "，旧密码：" + passOld + "，新密码：" + passNew);
		}
	}
	
}
