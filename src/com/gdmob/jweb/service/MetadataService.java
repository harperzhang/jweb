package com.gdmob.jweb.service;

import java.util.List;

import org.apache.log4j.Logger;

import com.gdmob.jweb.model.MetaData;

public class MetadataService extends BaseService {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(MetadataService.class);

	public static final MetadataService service = new MetadataService();
	
	public List<MetaData> getMetaDatasByType(String type){
		return MetaData.dao.getMetaDatasByType(type);
	}
	public void saveMetaData(MetaData metaData){
		metaData.save();
	}
	public MetaData getColorMetaData(String value){
		return MetaData.dao.getMetaData(value,value,MetaData.TYPE_COLOR);
	}
	public MetaData getChainMetaData(String value){
		return MetaData.dao.getMetaData(value,value,MetaData.TYPE_CHAIN);
	}
}
