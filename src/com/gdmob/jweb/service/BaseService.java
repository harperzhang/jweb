package com.gdmob.jweb.service;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.gdmob.jweb.common.SplitPage;
import com.gdmob.jweb.config.Constants;
import com.gdmob.jweb.model.BaseModel;
import com.gdmob.jweb.model.Upload;
import com.gdmob.jweb.plugin.PropertiesPlugin;
import com.gdmob.jweb.tools.ToolDateTime;
import com.gdmob.jweb.tools.ToolSqlXml;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;

public abstract class BaseService {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(BaseService.class);

	/**
	 * 根据i18n参数查询获取哪个字段的值
	 * @param i18n
	 * @return
	 */
	protected String i18n(String i18n){
		return BaseModel.i18n(i18n);
	}
	
	/**
	 * 把11,22,33...转成'11','22','33'...
	 * @param ids
	 * @return
	 */
	protected String toSql(String ids){
		if(null == ids || ids.trim().isEmpty()){
			return null;
		}
		
		String[] idsArr = ids.split(",");
		StringBuilder sqlSb = new StringBuilder();
		int length = idsArr.length;
		for (int i = 0, size = length -1; i < size; i++) {
			sqlSb.append(" '").append(idsArr[i]).append("', ");
		}
		if(length != 0){
			sqlSb.append(" '").append(idsArr[length-1]).append("' ");
		}
		
		return sqlSb.toString();
	}

	/**
	 * 把数组转成'11','22','33'...
	 * @param ids
	 * @return
	 */
	protected String toSql(String[] idsArr){
		if(idsArr == null || idsArr.length == 0){
			return null;
		}
		
		StringBuilder sqlSb = new StringBuilder();
		int length = idsArr.length;
		for (int i = 0, size = length -1; i < size; i++) {
			sqlSb.append(" '").append(idsArr[i]).append("', ");
		}
		if(length != 0){
			sqlSb.append(" '").append(idsArr[length-1]).append("' ");
		}
		
		return sqlSb.toString();
	}

	/**
	 * 分页
	 * @param splitPage
	 * @param select
	 * @param sqlId
	 */
	protected void splitPageBase(SplitPage splitPage, String select, String sqlId){
		// 接收返回值对象
		StringBuilder formSqlSb = new StringBuilder();
		LinkedList<Object> paramValue = new LinkedList<Object>();
		
		// 调用生成from sql，并构造paramValue
		String sql = ToolSqlXml.getSql(sqlId, splitPage.getQueryParam(), paramValue);
		formSqlSb.append(sql);
		
		// 行级：过滤
		rowFilter(formSqlSb);
		
		// 排序
		String orderColunm = splitPage.getOrderColunm();
		String orderMode = splitPage.getOrderMode();
		if(null != orderColunm && !orderColunm.isEmpty() && null != orderMode && !orderMode.isEmpty()){
			formSqlSb.append(" order by ").append(orderColunm).append(" ").append(orderMode);
		}
		
		String formSql = formSqlSb.toString();

		Page<?> page = Db.paginate(splitPage.getPageNumber(), splitPage.getPageSize(), select, formSql, paramValue.toArray());
		splitPage.setPage(page);
	}
	protected void splitPage(SplitPage splitPage, String select, String sql){
		// 接收返回值对象
		StringBuilder formSqlSb = new StringBuilder();
		LinkedList<Object> paramValue = new LinkedList<Object>();
		
		// 调用生成from sql，并构造paramValue
		//String sql = ToolSqlXml.getSql(sqlId, splitPage.getQueryParam(), paramValue);
		formSqlSb.append(sql);
		
		// 行级：过滤
		rowFilter(formSqlSb);
		
		// 排序
		String orderColunm = splitPage.getOrderColunm();
		String orderMode = splitPage.getOrderMode();
		if(null != orderColunm && !orderColunm.isEmpty() && null != orderMode && !orderMode.isEmpty()){
			formSqlSb.append(" order by ").append(orderColunm).append(" ").append(orderMode);
		}
		
		String formSql = formSqlSb.toString();

		Page<?> page = Db.paginate(splitPage.getPageNumber(), splitPage.getPageSize(), select, formSql, paramValue.toArray());
		splitPage.setPage(page);
	}
	/**
	 * 行级：过滤
	 * @return
	 */
	protected void rowFilter(StringBuilder formSqlSb){
		
	}
	protected String getDestFile(UploadFile uploadFile,String type){
		String typeDir = type+File.separator+ToolDateTime.getCurrentYmd();
		String dir = (String) PropertiesPlugin.getParamMapValue(Constants.DATA_DIR)+File.separator+typeDir;
		File destDir = new File(dir);
		if(!destDir.exists()){
			destDir.mkdirs();
		}
		String fileName = uploadFile.getFileName();
		if(type.equals(Constants.SOFT)){
			fileName = uploadFile.getOriginalFileName();
		}
		File destFile = new File(destDir,fileName);
		try {
			FileUtils.moveFile(uploadFile.getFile(),destFile);
		} catch (IOException e) {
			
		}
		String filePath = typeDir + File.separator + fileName;
		Upload upload = new Upload();
		upload.set("type",type);
		upload.set("filename",fileName);
		upload.set("contenttype",uploadFile.getContentType());
		upload.set("originalfilename",uploadFile.getOriginalFileName());
		filePath.replace("\\","/");
		upload.set("path",filePath);
		upload.save();
		String ids = upload.getStr("ids");
		return ids;
	}
}
