package com.gdmob.jweb.handler;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.gdmob.jweb.common.DictKeys;
import com.gdmob.jweb.plugin.I18NPlugin;
import com.gdmob.jweb.plugin.PropertiesPlugin;
import com.gdmob.jweb.tools.ToolDateTime;
import com.gdmob.jweb.tools.ToolWeb;
import com.jfinal.handler.Handler;

/**
 * 全局Handler，设置一些通用功能
 * 描述：主要是一些全局变量的设置，再就是日志记录开始和结束操作
 */
public class GlobalHandler extends Handler {
	
	private static Logger log = Logger.getLogger(GlobalHandler.class);

	public static final String reqSysLogKey = "reqSysLog";
	
	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		log.info("访问路径:"+target);
		//设置 web 路径
		String cxt = ToolWeb.getContextPath(request);
		request.setAttribute("cxt", cxt);
		
		//request cookie 处理
		Map<String, Cookie> cookieMap = ToolWeb.readCookieMap(request);
		request.setAttribute("cookieMap", cookieMap);

		//request param 请求参数处理
		request.setAttribute("paramMap", ToolWeb.getParamMap(request));
		//request 国际化
		String localePram = request.getParameter("localePram");
		if(null != localePram && !localePram.isEmpty()){
			int maxAge = (int) PropertiesPlugin.getParamMapValue(DictKeys.config_maxAge_key);
			ToolWeb.addCookie(response,  "", "/", true, "language", localePram, maxAge);
		}else {
			localePram = ToolWeb.getCookieValueByName(request, "language");
			if(null == localePram || localePram.isEmpty()){
				Locale locale = request.getLocale();
				String language = locale.getLanguage();
				localePram = language;
				String country = locale.getCountry();
				if(null != country && !country.isEmpty()){
					localePram += "_" + country;
				}
			}
		}
		localePram = localePram.toLowerCase();
		Map<String, String> i18nMap = I18NPlugin.get(localePram);
		request.setAttribute("localePram", localePram);
		request.setAttribute("i18nMap", i18nMap);
		
		//设置Header"
		request.setAttribute("decorator", "none");
		response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
		response.setHeader("Pragma","no-cache"); //HTTP 1.0
		response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
		
		nextHandler.handle(target, request, response, isHandled);
	}
}
