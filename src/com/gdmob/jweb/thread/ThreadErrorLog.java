package com.gdmob.jweb.thread;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;

import com.gdmob.jweb.model.ErrorLog;

/**
 * 操作日志入库处理
 */
public class ThreadErrorLog {

	private static Logger log = Logger.getLogger(ThreadErrorLog.class);
	
	private static final int queueSize = 5000; // 入库队列大小
	private static boolean threadRun = true; // 线程是否运行
	
	/**
	 * 队列,此队列按照 FIFO（先进先出）原则对元素进行排序。
	 */
	private static Queue<ErrorLog> queue = new ConcurrentLinkedQueue<ErrorLog>();
	public static void setThreadRun(boolean threadRun) {
		ThreadErrorLog.threadRun = threadRun;
	}
	/**
	 * 向队列中增加Syslog对象，基于ConcurrentLinkedQueue
	 * @param errorlog
	 */
	public static void add(ErrorLog errorLog){
		if(null != errorLog){	// 此队列不允许使用 null 元素
			synchronized(queue) {
				if(queue.size() <= queueSize){
					queue.offer(errorLog);
				}else{
					queue.poll(); // 获取并移除此队列的头，如果此队列为空，则返回 null
					queue.offer(errorLog); // 将指定元素插入此队列的尾部
					log.error("日志队列：超过" + queueSize);
				}
			}
		}
	}
	
	/**
	 * 获取ErrorLog对象，基于ConcurrentLinkedQueue
	 * @return
	 */
	public static ErrorLog getErrorLog(){
		synchronized(queue) {
			if(queue.isEmpty()){
				return null;
			}else{
				return queue.poll(); // 获取并移除此队列的头，如果此队列为空，则返回 null
			}
		}
	}
	
	/**
	 * 启动入库线程
	 */
	public static void startSaveDBThread() {
		try{
			for(int i = 0; i < 10; i++){
				Thread insertDbThread = new Thread(new Runnable() {
					public void run(){
						while (threadRun) {
							try {
								// 取队列数据
								ErrorLog errorLog = getErrorLog();
								if(null == errorLog){
									Thread.sleep(200);
								}else{
									errorLog.saveModel();// 日志入库
								}
							} catch (Exception e) {
								throw new RuntimeException("ThreadErrorLog -> save Exception");
							}
						}
					}
				});
				insertDbThread.setName("logerror-" + (i + 1));
				insertDbThread.start();
			}
		}catch(Exception e){
			throw new RuntimeException("ThreadErrorLog new Thread Exception");
		}
	}
}
