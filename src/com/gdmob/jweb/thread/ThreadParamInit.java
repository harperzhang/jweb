package com.gdmob.jweb.thread;

import java.util.List;

import org.apache.log4j.Logger;

import com.gdmob.jweb.model.Group;
import com.gdmob.jweb.model.Operator;
import com.gdmob.jweb.model.Role;
import com.gdmob.jweb.model.User;
import com.gdmob.jweb.tools.ToolSqlXml;

/**
 * 系统初始化缓存操作类
 */
public class ThreadParamInit extends Thread {
	
	private static Logger log = Logger.getLogger(ThreadParamInit.class);
	
	public static String cacheStart_user = "user_";
	public static String cacheStart_group = "group_";
	public static String cacheStart_role = "role_";
	public static String cacheStart_operator = "operator_";
	public static String cacheStart_dict = "dict_";
	public static String cacheStart_dict_child =  "dict_child_";
	public static String cacheStart_param = "param_";
	public static String cacheStart_param_child =  "param_child_";
	
	@Override
	public void run() {
		log.info("缓存参数初始化 start ...");
		// 1.缓存用户
		cacheUser();

		// 2.缓存组
		cacheGroup();

		// 3.缓存角色
		cacheRole();

		// 5.缓存功能
		cacheOperator();
		log.info("缓存参数初始化 end ...");
	}

	/**
	 * 缓存所有用户
	 */
	public static void cacheUser() {
		String sql = ToolSqlXml.getSql("pingtai.user.all");
		List<User> userList = User.dao.find(sql);
		for (User user : userList) {
			User.dao.cacheAdd(user.getStr("ids"));
			user = null;
		}
		userList = null;
	}

	/**
	 * 缓存所有组
	 */
	public static void cacheGroup() {
		String sql = ToolSqlXml.getSql("pingtai.group.all");
		List<Group> groupList = Group.dao.find(sql);
		for (Group group : groupList) {
			Group.dao.cacheAdd(group.getStr("ids"));
		}
		groupList = null;
	}

	/**
	 * 缓存所有角色
	 */
	public static void cacheRole() {
		String sql = ToolSqlXml.getSql("pingtai.role.all");
		List<Role> roleList = Role.dao.find(sql);
		for (Role role : roleList) {
			Role.dao.cacheAdd(role.getStr("ids"));
		}
		roleList = null;
	}
	/**
	 * 缓存操作
	 */
	public static void cacheOperator() {
		String sql = ToolSqlXml.getSql("pingtai.operator.all");
		List<Operator> operatorList = Operator.dao.find(sql);
		for (Operator operator : operatorList) {
			Operator.dao.cacheAdd(operator.getStr("ids"));
			operator = null;
		}
		operatorList = null;
	}
}
