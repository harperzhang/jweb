package com.gdmob.jweb.controller;

import java.util.List;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Controller;
import com.gdmob.jweb.model.Menu;
import com.gdmob.jweb.model.User;
import com.gdmob.jweb.service.IndexService;
import com.gdmob.jweb.service.ResourcesService;
import com.gdmob.jweb.tools.ToolContext;
import com.gdmob.jweb.tools.ToolSqlXml;

/**
 * 首页处理
 */
@SuppressWarnings("unused")
@Controller(controllerKey = {"/jw/", "/jw/index"})
public class IndexController extends BaseController {

	private static Logger log = Logger.getLogger(IndexController.class);
	
	private List<Menu> menuList;
	
	/**
	 * 首页
	 */
	public void index() {
		User user = ToolContext.getCurrentUser(getRequest(), true); // cookie认证自动登陆处理
		if(null != user){//后台
			menuList = IndexService.service.menu( user, getI18nPram());
			render("/pingtai/index.html");
		}else{
			render("/pingtai/login.html");
		}
	}
	
	/**
	 * 首页content
	 */
	public void content(){
		render("/pingtai/content.html");
	}
	
}
