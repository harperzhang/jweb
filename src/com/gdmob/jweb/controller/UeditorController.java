package com.gdmob.jweb.controller;

import java.io.File;

import org.apache.log4j.Logger;

import com.baidu.ueditor.ActionEnter;
import com.gdmob.jweb.annotation.Controller;
import com.jfinal.kit.PathKit;

/**
 * Ueditor
 */
@SuppressWarnings("unused")
@Controller(controllerKey = {"/jw/ueditor"})
public class UeditorController extends BaseController {

	private static Logger log = Logger.getLogger(UeditorController.class);
	
	public void index() {
		String htmlText = new ActionEnter( getRequest(), PathKit.getWebRootPath() + File.separator ).exec();
		renderHtml(htmlText);
	}
	
}
