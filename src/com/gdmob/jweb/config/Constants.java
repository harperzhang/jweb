package com.gdmob.jweb.config;

import java.io.File;

import com.jfinal.kit.PathKit;

public class Constants {
	public static final String JWEB_HOME = "jweb.home";
	public static final String CONFIG_FILE = "init.properties";
	public static final String DEFAULT_PROPERTY_PATH = PathKit.getWebRootPath() 
			+ File.separator + "WEB-INF" 
			+ File.separator + Constants.CONFIG_FILE;
	public static final String LOG4J_CONFIG_FILE = "log4j.properties";
	public static final String DEFAULT_LOG4J_FILE = "/WEB-INF/classes/"+LOG4J_CONFIG_FILE;
	public static final String LOGS_DIR = "logs";
	public static final String DEFAULT_LOGS_PATH = "/WEB-INF/" + LOGS_DIR;
	public static final String DEFAULT_JWEB_HOME_DIR = "F:/gdmob/do/jweb/home";
	public static final String DEFAULT_JWEB_DATA_DIR = "F:/gdmob/do/jweb/home/data";
	
	public static final String DATA_DIR = "data.dir";
	public static final String SOFT = "soft";
	public static final String ACCOUNT = "account";
	public static final String UEDITOR = "ueditor";
	public static final String NGINX_URL = "nginx.url";
	public static final String DEFAULT_NGINX_URL = "http://localhost:8787";
}
